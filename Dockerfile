FROM library/debian:bullseye-slim
COPY webdav.conf /webdav.conf
RUN apt -y update &&\
 apt -y upgrade &&\
 apt -y install apache2 &&\ 
 a2enmod auth_basic authn_file authz_core authz_host authz_user socache_shmcb rewrite deflate headers auth_digest dav dav_fs mpm_event autoindex &&\ 
 a2dissite {000-default,default-ssl} &&\ 
 echo 'RequestHeader edit Destination ^https http early' > /etc/apache2/conf-enabled/webdav-fix.conf &&\ 
 mv /webdav.conf /etc/apache2/sites-available/webdav.conf &&\ 
 a2ensite webdav
CMD [ "bash", "-c", "/usr/sbin/apache2ctl -k start && sleep 10 && while [ -e /var/run/apache2/apache2.pid ] && [ -e /proc/$(</var/run/apache2/apache2.pid) ]; do sleep 1; done" ]
EXPOSE 80/tcp
